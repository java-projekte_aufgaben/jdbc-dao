# JDBC & DAO



# Inhaltsverzeichnis

- [JDBC & DAO](#jdbc--dao)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Entwicklungsumgebung einrichten (Video 1)](#entwicklungsumgebung-einrichten-video-1)
  - [Spaß Java Database Connectivity JDBC (Video 2)](#spaß-java-database-connectivity-jdbc-video-2)
  - [Datenbankverbindung herstellen Teil 2 (Video 3)](#datenbankverbindung-herstellen-teil-2-video-3)
  - [Daten abfragen (Video 4)](#daten-abfragen-video-4)
  - [Daten eingügen (Video 5)](#daten-eingügen-video-5)
  - [Daten aktualisieren (Video 6)](#daten-aktualisieren-video-6)
  - [Datensätze löschen (Video 7)](#datensätze-löschen-video-7)
  - [Datensätze abfragen Teil 2 (Video 8)](#datensätze-abfragen-teil-2-video-8)
- [Spaß mit JDBC und DAO (Video 1)](#spaß-mit-jdbc-und-dao-video-1)
  - [Projektsetup (Video 2)](#projektsetup-video-2)
  - [Datenbankverbindung mit Singleton (Video 3)](#datenbankverbindung-mit-singleton-video-3)
  - [Kommandozeilenmenü (Video 4)](#kommandozeilenmenü-video-4)
  - [Domänenklassen (Video 5)](#domänenklassen-video-5)
  - [DAO Interfaces (Video 6)](#dao-interfaces-video-6)
    - [**Base Repository**](#base-repository)
    - [**MyCourseRepository**](#mycourserepository)
  - [DAO Implementierung Teil 1 - CRUD (Video 7)](#dao-implementierung-teil-1---crud-video-7)
    - [**MySqlCourseRepository**](#mysqlcourserepository)
    - [**CLI überarbeitet**](#cli-überarbeitet)
  - [GetAll Teil 2 (Video 7 teil 2)](#getall-teil-2-video-7-teil-2)
  - [DAO Implementierung  Teil 2 - CRUD continued (Video 8)](#dao-implementierung--teil-2---crud-continued-video-8)
  - [DAO Implementierung CRUD - CREATE](#dao-implementierung-crud---create)
  - [DAO Implementierung CRUD - Update (Video 10)](#dao-implementierung-crud---update-video-10)
  - [DAO Implementierung CRUD - DELETE (Video 11)](#dao-implementierung-crud---delete-video-11)
  - [DAO Implementierung Kurssuche (Video 12)](#dao-implementierung-kurssuche-video-12)
  - [DAO Implementierung Aktuell laufende Kurse (Video 13)](#dao-implementierung-aktuell-laufende-kurse-video-13)
- [AUFGABE 4: JDBC UND DAO – STUDENTEN](#aufgabe-4-jdbc-und-dao--studenten)
  - [Student Klasse](#student-klasse)
  - [MySqlRepository Klasse](#mysqlrepository-klasse)
  - [MySqlRepository Interface](#mysqlrepository-interface)
  - [Cli erweitert](#cli-erweitert)
- [AUFGABE 5: JDBC UND DAO – BUCHUNGEN](#aufgabe-5-jdbc-und-dao--buchungen)








## Entwicklungsumgebung einrichten (Video 1)
 + Webserver
 + Datenbankserver 
 + IDE
 + Maven-Projektsetup

Zuerst wird Xampp installiert, das habe ich schon seit dem ersten Semester. Anschließend wird ein neues **Maven** nicht **java!!** Projekt erstellt. Nach dem erstellen wird eine Konfigurationsdatei für die Dependencies bzw. Abhängigkeiten sowie diverse Bibliotheken autogeneriert.

Die gewünschten Dependencies können unter:
https://mvnrepository.com 
heruntergeladen werden. Dort einfach im Suchfeld die gewünschte Dependencie eingeben und kopieren.

Maven ist ein Buildtool um Java Projekte zu kompilieren, bauen sowie testen bzw. zu Packages daraus zu generieren. Weiteres wird das Maventool gebraucht um die Dependencies zu managen sprich alle Abhängigkeiten zu anderen Bibliotheken.

<br>

## Spaß Java Database Connectivity JDBC (Video 2)
<br>
Durch XAMPP wied die Verbindung zu Datenbank hergestellt und als ersten Schritt wird eine neue Datenbank erstellt namens **jdbcdemo**. Die Datenbank verfügt über eine Tabelle namens **student** mit 3 Spalten(id, name, email).

![Datenbank_erstellen](images/Datenbank_erstellen.png)


Als nächstes werden Daten in die student Tabelle eingetraen.
Angezigt wird stehts der SQL Befehl.

![students_eintragen](images/students_anlegen.png))


Dieses INSERT INTO wird als Kommentar in unser jdbcdemo Projekt in InteliJ gepeichert da wir diese stament später über java absetzen.

![Kommentar](images/Kommentar_statement.png)


Test eines SELECT Befehles.

![Select](images/Select.png)


<br>

## Datenbankverbindung herstellen Teil 2 (Video 3)
<br>
In diesem Teil wird die Verbindung zur Datenbank über java hersgestellt. In der Main Klasse wird als Erstes eine neue Methode public static void **selectAllDemo() **erstellt die uns alle Daten aus der Datenbank liefert.
Diese Methode gibt speichert ein **SELEC statement** in die Variable **sqlSelectAllPersons** welche vom Typ String ist.
Dannach wird die Verbindung bzw. die URL definiert mit einer weiteren String Variable namens **connetionUrl**. Diese wird mit dem standartpfad initialisiert (wichtig! Richtigen DB namen angeben). 
Um eine Verbidnung herstellen zu können wird ein **try catch block** definiert, dort können **SQL Exceptions** auftreten. Im **Try** blcok wird die Verbindung hergestellt und sollte dies erfolgreich sein wird (Verbindung zur Datenbank hergestellt) ausgegeben andernfalls die Fehlermeldung (Exception).
  
+ try = wird eine Ressource verbunden (Connection con...)
+ catch = wird die Fehlermeldung bzw. Exception geschmissen.

```java
    public static void selectAllDemo(){

        System.out.println("Select DEMO mit JDBC");
        String sqlSelectAllPersons = "Select * FROM `student`";
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";

        try(Connection con = DriverManager.getConnection(connectionUrl, "root", "")){

            System.out.println("Verbindung zur Datenbank hergestellt");
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur Datenbank" + e.getMessage());
        }
    }
```

<br>
Wichtig! localhost:3306 = Datenbankserver & jdbcdemo = Datenbankname
<br>

Besser ist es Variablen mit vordefiniertgen Datenwerten zu verwenden.

```java
    public static void selectAllDemo(){

        System.out.println("Select DEMO mit JDBC");
        String sqlSelectAllPersons = "Select * FROM `student`";
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";

        try(Connection con = DriverManager.getConnection(connectionUrl, user, pwd)){

            System.out.println("Verbindung zur Datenbank hergestellt");
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur Datenbank" + e.getMessage());
        }
    }
```


## Daten abfragen (Video 4)
<br>

Über den DriverManager wird eine Verbindung zur Datenbank versucht herzustellen auf Basis der **URL**, den **User**, dem **Passwort** pwd. Dieses Connectionobjekt wird in die Variable **conn** gespeichtert.
Das alles muss in einem **Try Catch block** eingrahmt sein weil immer SQL Exceptions auftreten können!

Anschließend wird eine Variable **PrepareStatement** vom Typ **PrepareStatement** erstellt. In diese Variabel wird die Verbindung sowie ein SQL Statement gespeichert welche an die DB gesendet wird.
Dannach wird eine Vairable **rs** vom Typ **ResultSet** welche uns das gewünschte Satement ausführt (durch **prepareStatement.executeQuery()**). Zurück kommt ein **ResultSet** über welches mit einer **while** Schleife iterriert wird sprich das ResultSet **(rs.next())** wird mit einem **Zeiger** durchlaufen und stoppt erst wenn **false** zurückgegeben wird. Also solange **true** ist(ein weiterer Datensatz zur Verfügung steht) wird die Schleife ausgeführt.
An der **rs** Variable können verschiedene Methoden aufgerufen werden(getInt(), getString()...) und die entsprechenden Datensätze aufgerufen werden.
```java

     try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur Datenbank hergestellt! ");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM `student`");
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next())
            {
             int id = rs.getInt("id");
             String name = rs.getString("name");
             String email = rs.getString("email");
                System.out.println("Student aus der Datenbank: [ID] " + id + " [NAME] " + name + " [EMAIL] " + email );
            }

```

<br>
## Daten eingügen (Video 5)
<br>
In diesem Teil werden Datensätze in unsere Datenbank hinzugefügt.
Der Anfang ist der selbe wie im letzten Abschnitt.

wichtige Aspekte dieses Videos:

 + In die Tabelle student wird ein **INSERT INTO Statement** ausgeführt für **id**, **name** und **email** wobei **id null gelassen** wird.
 + Für name und email werden **?** verwendet (Sicherheit --> SQL Injection,  perfomance).
 + Daruas wird das **PreparedStatement** vorbereitet.
 + An diesem **PreparedStatement** können dann die Methoden aufgerufen werden (**.setString()**...) und für die **?** die Werte angegeben werden (**parameterindex 1 = Erstes ? also name**, **parameterindex 2 = Zweites ? also die email**).
 + Die Variable **rowAffected** vom typ **int** zeigt die eingefügten Datensätze werlche betroffen sind!
 + Dieser Bereich wird in einem eigenen Try Catch Block geglieder um festzustellen ob die Verbindung oder das Insert Satement betroffen ist.

```java
    public static void insertStudentDemo()
    {
        System.out.println("INSERT DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";


        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur Datenbank hergestellt! ");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL,?,? )"
            );

            try
            {
                preparedStatement.setString(1,"Peter Zeck");
                preparedStatement.setString(2, "p.zeck@hotmail.com");
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze eingefügt");
            }catch (SQLException ex)
            {
                System.out.println("Fehler im SQL-INSERT Statement: " + ex.getMessage());
            }


        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur Datenbank " + e.getMessage());
        }

    }

```

**Wichtige Bemerkung:**
Die Verbindung wird in die Runden Klammern des Try() blockes geschreiben damit die Verbindung automatisch wieder geschlossen wird sobald der Try block abgeschlossen wurde!!

<br>

## Daten aktualisieren (Video 6)
<br>

Um Daten zu ändern oder Aktualiseren wid das UPDATE Statement verwendet.
Dies kann ebenfalls direkt in phpMyAdmin gemacht werden.

Bsp:
![Name-ändern-gui](images/Name-ändern-gui.png)

In unserem Projekt sieht das so aus.
Sehr ähnlich wie die Methoden zuvor. 
änderung hier ist das UPDATE Statement.
+ **UPDATE** des **name** in der Tabelle.
+ **id** wird hier noch angegeben damit nicht alle Namen geändert werden.

```java
  public static void updateStudentDemo()
    {
        System.out.println("Update DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";


        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur Datenbank hergestellt! ");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "UPDATE `student` SET `name` = ? WHERE `student`.`id` = 4"
            );

            try
            {
                preparedStatement.setString(1, "Hans Zimmer");
                int affectedRows = preparedStatement.executeUpdate();
                System.out.println("Anzahl der aktualisierten Datensätze: " + affectedRows);

            }catch (SQLException ex)
            {
                System.out.println("Fehler im SQL-UPDATE Statement: " + ex.getMessage());
            }


        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur Datenbank " + e.getMessage());
        }

    }
```

**Ein weiteres Update**

![waldhart-update](images/waldhart-update.png)

**Änderung im Code**

```java
     PreparedStatement preparedStatement = conn.prepareStatement(
                    "UPDATE `student` SET `name` = ? , `email` = ? WHERE `student`.`id` = 5"
            );

            try
            {
                preparedStatement.setString(1, "Peter Zweck");
                preparedStatement.setString(2, "p.zweck-waldhart@hotmail.com");
                int affectedRows = preparedStatement.executeUpdate();
                System.out.println("Anzahl der aktualisierten Datensätze: " + affectedRows);
```
<br>

## Datensätze löschen (Video 7)
<br>

Auch zum Löschen von Datensätzen kann phpMyAdmin verwednet werden um dierekt Datensätze zu löschen oder das benötigte SQL Statement zu generieren.

![Delete-Satement](images/Delete-myadmin.png)

Diese Methode dient zum Löschen von Datensätzen und ist wieder ähnlich zu den vorherigen Methoden.

**Wichtiges:**
+ Methodenparameter studentId vom Typ int damit ? bei id angegeben werden kann bzw. in der Main wird die gewünschte id angegeben. (An allen Methoden geändert)
+ DELETE Statement
```java
    public static void deleteStudentDemo(int studentId)
    {
        System.out.println("Delete DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";


        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur Datenbank hergestellt! ");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "DELETE FROM `student` WHERE `student`.`id` = ?"
            );

            try
            {
                preparedStatement.setInt(1, studentId);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println("Anzahl der gelöschten Datensätze: " + rowAffected);
            }catch (SQLException ex)
            {
                System.out.println("Fehler im SQL-DELETE Statement: " + ex.getMessage());
            }


        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur Datenbank " + e.getMessage());
        }
    }
```

<br>

## Datensätze abfragen Teil 2 (Video 8)
<br>

In diesem Teil wird eine Methode erstellt die einen Student mit einem bestimmten namen findet. Auch dies kann wieder im phpMyAdmin direkt erledigt werden.

![FindBy](images/findBy.png)


**Wichtig:**
+ **LIKE** Operatior ist nicht Casesensitive!
+ bei **preparedStatement.setString( parameterindex 1, x: "%"+name+"%")** die **% Wildcards** dienen der findung der Darauffolgenden Buchstaben die mit dem Parameter **name** gesetzt werden.

```java
private static void findAllByNameLike(String pattern)
    {
        System.out.println("Find all by Name DEMO mit JDBC");
        String sqlSelectAllPersons = "Select * FROM `student`";
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";

        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur Datenbank hergestellt! ");

            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT * FROM `student` WHERE `student`.`name` LIKE ?"
            );
            preparedStatement.setString(1, "%"+ pattern+"%");
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next())
            {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der Datenbank: [ID] " + id + " [NAME] " + name + " [EMAIL] " + email );
            }

        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur Datenbank " + e.getMessage());
        }

    }
```

<br>

# Spaß mit JDBC und DAO (Video 1)
<br>

**JDBC = Java DataBase Connectivity**
<br>

**DAO = Data Access Objekt**

Dao ist ein Entwurfsmuter, das den Zugrgiff auf unterschiedliche Arten von Datenquellen (z.B. Datenbanken, Dateisystem) so kapselt, dass die angesprochene Datenquelle ausgetauscht werden kann, ohne dass der aufrufende Code geändert werden muss. Dadurch soll die eigentliche Programmlogik von technischen Details der Datenspeicherung befreit werden und flexibler einsetzbar sein.

<br>

## Projektsetup (Video 2)

Zuerst wird eine neue Datenbak namens kurssystem erstellt. Diese Datenbank besitzt soweit nur eine Tabelle mit 7 Spalten (id, name, description, hours, begindate, enddate, coursetype)

![DB-erstellen](image%20courses/DB-erstellen.png)

<br>

![DB-ansicht](image%20courses/DB-ansicht.png)

<br>

Anschließend wird ein neues Maven Projekt namens **coursesystem** angelegt.

![Projektstrart](image%20courses/Projektstart.png)


<br>

## Datenbankverbindung mit Singleton (Video 3)
<br>
Das Singleton-Muster ist eines der einfachsten Entwurfsmuster in Java. Diese Art von Entwurfsmuster fällt unter Kreationsmuster, da dieses Muster eine der besten Mäglichkeiten  zum Erstellen eines Objektes bietet.
Dieses Muster beinhaltet einzelne Klasse, die dafür verantwortlich ist, ein Objekt zu erstellen, während sichergestellt wird, dass nur ein einzelnes Objekt erstellt wird. Diese Klasse bietet eine Möglichkeit, auf ihr eigenes Objekt zuzugreifen, auf das direkt zugegriffen werden kann, ohne dass das Objekt der Klasse instanziert werden muss.


Im Projekt wird ein neues Package angelegt namens dataaccess in der sich die Klasse MysqlDatabaseConneciton befindet.
Diese Klasse verwendet das Singleton Pattern.
Ein statisches Datenfeld von Typ Connection das mit null initialisiert wird.
Darunter befindet sich ein privater Konstruktor sowie die getConnection Methode welche für die Verbindung zur Datenbank zuständig ist. Es werden 3 Parameter mitgegeben(url, userm pwd).
Diese Methode prüft ob ein Connection Objekt bereits existiert falls nicht wird eines erzeugt und falls doch wird das bestehende Objekt ausgegeben.

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class MysqlDatabaseConnection {

    private static Connection con = null;

    private MysqlDatabaseConnection()
    {

    }

    public static Connection getConnection(String url, String user, String pwd) throws ClassNotFoundException, SQLException {
        if(con != null){
            return con;
        } else {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url,user,pwd);
            return con;
        }
    }
```

<br>

In der Main wird dann die Verbindung in einem Try Catch block hergestellt um sicherzustellen das bei der Laufzeit nur jeweils  1 instanz von einer bestimmten Klasse bekommt sprich man bekommt immer die selbe Connection instanz egal wie oft der  getConnection aufgerufen wird.

```java
public class Main {

    public static void main(String[] args) {


        try {
            Connection myConnection = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");
            System.out.println("Verbindung aufgebaut");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
```

<br>

## Kommandozeilenmenü (Video 4)

In diesem Teil wird ein neues Package namens ui erstellt (User Interface) indem sich die Klasse Cli (Commandline Interface) befindet welech Benutzereingaben entgegen nimmt.

```java
package ui;

import java.util.Scanner;

public class Cli {

    Scanner scan;

    public Cli()
    {
        this.scan = new Scanner(System.in);
    }


    public void start()
    {
        String input = "-";
        while (!input.equals("x"))
        {
            showMenue();
            input = scan.nextLine();
            switch(input){
                case "1":
                    System.out.println("Kurseingabe");
                    break;
                case "2":
                    System.out.println("Alle Kurse anzeigen");
                    break;
                case "x":
                    System.out.println("Auf Wiedersehen!");
                    break;
                default:
                    inputError();
                    break;
            }
        }
        scan.close();
    }

    private void showMenue()
    {
        System.out.println("------------------------ KURSMANAGEMENT ------------------------");
        System.out.println("(1) Kurs eingeben \t (2) Alle Kurse anzeigen \t");
        System.out.println("(x) Ende");
    }


    private void inputError()
    {
        System.out.println("Bitte nur die Zahlen der Menüauswahl eingeben");
    }
}
```

<br>

## Domänenklassen (Video 5)
<br>

Im Package domain befinden sich unsere Domänenklassen.


Von dieser Klasse (BaseEntity) kann keine Instanz gebildet werden durch abstrakt
jedoch kann die Course Klasse, die sich ebenfalls im domain Package befindet, von BaseEntity erben durch extend. Somit erbt die Course Klasse alles was die Basisklasse auch besitzt. Es befinden sich 2 Konstruktoren. Bei beiden wird der Konstruktor der Mutterklasse aufgerufen wobei einmal die id und einmal null gesetzt wird. Ansonsten werden die Setter aufgerufen. Beide Konstruktoren werfen gegebenenfalls Exceptions, da die Setter verwendet werden.
Wichtig:
+ mit id = UPDATE, DELETE;
+ ohne id (null) = bei INSERT

```java
package domain;

import java.sql.Date;

public class Course extends BaseEntity {


    private String name;
    private String description;
    private int hours;
    private Date beginDate;
    private Date endDate;
    private CourseType courseType;


    public Course(Long id, String name, String description, int hours, Date beginDate, Date endDate, CourseType courseType) throws InvalidValueException {
        super(id);
        this.setName(name);
        this.setDescription(description);
        this.setHours(hours);
        this.setBeginDate(beginDate);
        this.setEndDate(endDate);
        this.setCourseType(courseType);
    }

    public Course( String name, String description, int hours, Date beginDate, Date endDate, CourseType courseType) throws InvalidValueException {
        super(null);
        this.setName(name);
        this.setDescription(description);
        this.setHours(hours);
        this.setBeginDate(beginDate);
        this.setEndDate(endDate);
        this.setCourseType(courseType);
    }
```
<br>
Die Setter der Klasse Course prüfen auf ob die Werte nicht null sind und führen weitere Prüfungen durch.
<br>
Wichtig:

+ Es werden bei allen Settern Exceptions geschmissen
+ beginDate.before() ist eine Methode der SQL Date Class.
+ endDate.after() ist ebenfalls eine SQL Date Class.


```java 
  public void setName(String name) throws InvalidValueException {
        if (name != null && name.length() > 1) {
            this.name = name;
        } else {
            throw new InvalidValueException("Kursname muss mindestens 2 Zeichen lang sein!");
        }
    }

    public void setDescription(String description) throws InvalidValueException {
        if (description != null && description.length() > 10) {
            this.description = description;
        } else {
            throw new InvalidValueException("Kursbeschreibung muss mindestens 10 Zeichen lang sein!");
        }
    }

    public void setHours(int hours) throws InvalidValueException {
        if (hours > 0 && hours < 10) {
            this.hours = hours;
        } else {
            throw new InvalidValueException("Anzahl der Kursstunden pro Kurs darf nur zwischen 1 und 10 liegen!");
        }
    }

    public void setBeginDate(Date beginDate) throws InvalidValueException {
        if (beginDate != null) {
            if (this.endDate != null) {
                if (beginDate.before(this.endDate)) {
                    this.beginDate = beginDate;
                } else {
                    throw new InvalidValueException("Kursbeginn muss VOR Kursende sein!");
                }
            } else {
                this.beginDate = beginDate;
            }
        } else {
            throw new InvalidValueException("Startdatum darf nicht null / leer sein");
        }
    }

    public void setEndDate(Date endDate) throws InvalidValueException {
        if (endDate != null) {
            if (this.beginDate != null) {
                if (endDate.after(this.beginDate)) {
                    this.endDate = endDate;
                } else {
                    throw new InvalidValueException("Kursende muss NACH Kursbeginn sein!");
                }
            } else {
                this.endDate = endDate;
            }
        } else {
            throw new InvalidValueException("Enddatum darf nicht null / leer sein");
        }
    }


    public void setCourseType(CourseType courseType) throws InvalidValueException {
        if (courseType != null) {
            this.courseType = courseType;
        } else {
            throw new InvalidValueException("Kurstyp darf nicht null / leer sein!");
        }
    }
```
<br>

Die abstrakte Basisklasse vom Typ BaseEntity. Sie ist eine Basisklasse für alle späteren Entitys die definiert werden.
Alle Entitys besitzen ein Datefeld id vom Typ Long.

Die Methode **setId** prüft ob die id **null** oder größer **0** ist
Trifft dies zu wird die **id** auch gesetzt ansosnsten wird eine **Exception** geschmissen.

**Grund für diese Prüfung:**
  + **ist die id null dient es uns als INSERT**
  + **ist die id größer 0 dient sie als UPDATE | DELETE**


```java
package domain;

public abstract class BaseEntity {

    public Long id;

    public BaseEntity(Long id)
    {
        setId(id);
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        if(id == null || id >= 0)
        {
            this.id = id;
        } else {
            throw new InvalidValueException("Kurs-ID muss größer gleich 0 sein!");
        }
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                '}';
    }
}
```
<br>

## DAO Interfaces (Video 6)
<br>

**REPOSITROY = DAO**

Beschreibung steht am Anfang!

Das Dao Interface dient als Schnittstelle zwischen der Cli Klasse und dem SQL DAO damit nicht direkt von Cli auf SQL zugegriffen werden kann.
Die Main greift auf die Cli Klasse zu.

Es wurde eine zweischichtige Interface Struktur im Dataaccess Package.
Die Beiden Interfaces sind **BaseRepository** & **MyCourseRepository**.

<br>

### **Base Repository**

Dieses Interface stellt und die **CRUD** Methoden zur Verfügung.
T Typ und I Typ sind momentan nur Platzhalter.

**IDEE VON OPTIONAL:** 
+ Optional ist ein Java typ der eine Wrapperklasse darstellt, (ein Packet in welchem etwas drin ist oder nicht!) sprich optional wird zurückgegeben!

Falls in der Datenbank das INSERT Statement erfolgreich war wird das mitgegebene Element wieder zurück nach ausen gegeben. Wenn nicht gibt es ein Optional
ohne Inhalt zurüück.
Alle unsere Repositorys bzw. DAO Implementierungen müssen inserten können und zwar mit einem beliebigen Generischen Typ T der eine Entity representiert.
In unserem Fall ist dies später ein Course Typ.
Zweiter Generischer Typ I wird verwendet um LONG mitgeben zu können kann aber auch ein anderer Referenztyp sein der diese Eindeutige Identität definiert in unserem FAll eben Long.

```java
package dataaccess;

import java.util.List;
import java.util.Optional;

public interface BaseRepository<T, I> {


    Optional<T> insert(T entity);
    Optional<T> getById(I id);
    List<T> getAll();
    Optional<T> update(T entity);
    void deleteById(I id);   
}

```

![DAO-Interface](image%20courses/DAO-Interface.png)

<br>

### **MyCourseRepository**
<br>
Dieses **Interface** dient als zusatz wo einerseits die Methoden des BaseRepository implementiert werden und andererseits können eigene **Zusatzmethoden** definiert werden.
Das BaseRepository dient als **CRUD** Repository deshalb wird erst in dieser Klasse **typisiert**!!
**Statt T jetzt Courses und I ist Long**. Abhängigkeiten werden vermieden und im weiteren Verlauf können weitere Repos mit anderen konkreten Typen implementiert werden.


```java
package dataaccess;

import domain.Course;
import domain.CourseType;

import java.sql.Date;
import java.util.List;

public interface MyCourseRepository extends BaseRepository <Course, Long>{

    List<Course> findAllCoursesByName(String name);
    List<Course> findAllCoursesByDescription(String description);
    List<Course> findAllCoursesByNameOrDescription(String searchText);
    List<Course> findAllCoursesByCourseType(CourseType courseType);
    List<Course> findAllCoursesByStartDate(Date startDate);
    List<Course> findAllRunnningCourses();

}
``` 

![DAO-extends](image%20courses/DAO-extends.png)

<br>

## DAO Implementierung Teil 1 - CRUD (Video 7)
<br>

### **MySqlCourseRepository**
<br>

In diesem Teil wird eine Klasse namens **MySqlCourseRepository** erstellt welche alle Methoden des **MyCourseRepository** & **BaseRepository** Interface implementiert.

Der Konstruktor dient zum Aufbau der Verbindung.

Die Methode **getAll()** dient zum ausgeben aller Kurse.
Sie verwendet den Konsturktor der Klasse **Course** in welchem die **id** gesetzt ist (der erste Kons.).
Die **Liste** ist entweder **leer** ansonsten wird über das **resultset** **itteriert** und **adden** in die Liste einen **neuen Course** (bei jedem Schleifendurchlauf)
mit den Daten die in jedem Durchlauf aus der Datenbank pro Zeile ausgegeben wird.
Falls ein Problem auftritt wird die Exception gecatched und eine eigene DatabaseException geschmissen.

```java
package dataaccess;


import domain.Course;
import domain.CourseType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class MySqlCourseRepository implements MyCourseRepository {

    private Connection con;

   
    public MySqlCourseRepository() throws SQLException, ClassNotFoundException
    {
       this.con = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");
    }

    @Override
    public Optional<Course> insert(Course entity) {
        return Optional.empty();
    }

    @Override
    public Optional<Course> getById(Long id) {
        return Optional.empty();
    }

    @Override
    public List<Course> getAll() {
        String sql = "SELECT * FROM `courses`";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next())
            {
                courseList.add( new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("enddate"),
                        CourseType.valueOf(resultSet.getString("coursetype"))
                        )
                );
            } 
            return courseList;
        } catch (SQLException e) {
            throw new DatabaseException("Database error occured!");
        }
        return null;
    }

    @Override
    public Optional<Course> update(Course entity) {
        return Optional.empty();
    }

    @Override
    public void deleteById(Long id) {}
    @Override
    public List<Course> findAllCoursesByName(String name) {
        return null;
    }

    @Override
    public List<Course> findAllCoursesByDescription(String description) {
        return null;
    }

    @Override
    public List<Course> findAllCoursesByNameOrDescription(String searchText) {
        return null;
    }

    @Override
    public List<Course> findAllCoursesByCourseType(CourseType courseType) {
        return null;
    }

    @Override
    public List<Course> findAllCoursesByStartDate(Date startDate) {
        return null;
    }

    @Override
    public List<Course> findAllRunningCourses() {
        return null;
    }
}

```
<br>

### **CLI überarbeitet**
<br >

Wichitges nebenbei:
+ Die Cli hält im wesentlichen eine Referenz auf dieses Interface und verwendet eine Implementierung von diesem Interface um z.B. Alle Daten aud der DB zu erhalten.
+ Im Konstuktor der Klasse CLi:
  + Gegeben ist eine Referenz auf MyCourseRepository sprich die Cli verwendet dieses Interface weiß jedoch nichts über die konkrete CourseRepository implementierung.
```java
package ui;

import dataaccess.MyCourseRepository;

import java.util.Scanner;

public class Cli {

    Scanner scan;
    MyCourseRepository repo;

    public Cli(MyCourseRepository repo)
    {
        this.scan = new Scanner(System.in);
        this.repo = repo;
    }

```

Es wird eine neue Liste erzeugt die Course hält und mit null initialisiert wird.
In die Variable list wird von der Mutterklasse die CRUD Methode getALL() aufgerufen und gespeichert.
Ist die größe der Liste größer 0 wird mit einer for Schleife darüber iteriert und in die course Variable gespeichert.
Anschließend wird die Liste mit den courses ausgegeben. Anderenfalls wird die Fehlermeldung ausgegeben bzw die Exception geschmissen.

```java
    private void showAllCourses() {

        List<Course> list = null;

        try{
        list = repo.getAll();
        if (list.size()>0)
        {
         for (Course course : list)
         {
             System.out.println(course);
         }
        } else {
            System.out.println("Kursliste leer!");
        }
    } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Anzeige aller Kurse: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Kurse: " + exception.getMessage());
        }
    }
```
<br>

## GetAll Teil 2 (Video 7 teil 2)
<br>

Wichtiger Punkt zu DAO Pattern:

+ ORM  = Objekt Relationales Mapping
+ Das Mappen von Zeilen aus der Datenbank in konkrete Course Objekte oder Listen von solchen Course Objekten.
+ Nicht nur das der Datenbankzugriff komplett weckextrahiert wird sondern auch das dieses Mapping generiert wird.
+ In unserem Bsp. passiert das Mapping im MySqlCourseRepository in der getAll() Methode
    + courseList.add(new Course(resultSet.getLong("id"))),...


<br>

## DAO Implementierung  Teil 2 - CRUD continued (Video 8)
<br>

Zuerst wird ein neues Package sowie eine neue Klasse namens Assert erstellt.
Die Assert Klasse übernimmt die null checks damit wir nicht bei jeder Methode diesen check durchführen müssen.

```java 
package util;

public class Assert {

    public static void notNull(Object o)
    {
        if (o == null) throw new IllegalArgumentException("Reference must not be null!");
    }

}
```
<br>

Nun wir die Methode **getById()** ausimplementiert.
Wichtig:
+ return Optional.of(course); Um ein Optional zu generieren kann auch die statische Methode .of() verwendet werden.
+ Dort wird das Objekt mitgegeben .of(course) das in das Optional verpackt werden soll und geben dies zurück nach ausen zweck Methodensignatur.

```java 
    @Override
    public Optional<Course> getById(Long id) {
        Assert.notNull(id);
        if (countCoursesInDbWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sql = "SELECT * FROM `courses` WHERE `id` = ?";
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();

                resultSet.next();
                Course course = new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("beginDate"),
                                resultSet.getDate("enddate"),
                                CourseType.valueOf(resultSet.getString("coursetype"))
                );
                return Optional.of(course);

            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

``` 

Eine weitere Hilfsmethode wird ebenfalls implementiert.
Diese Methode gibt die Anzahl von Datensätzen zu einer gegebenen id aus.
    
```java
   
    private int countCoursesInDbWithId(Long id)
    {
        try {
            String countSql = "SELECT COUNT(*) FROM `courses` WHERE `id`=?";
            PreparedStatement preparedStatementCount = con.prepareStatement(countSql);
            preparedStatementCount.setLong(1, id);
            ResultSet resultSetCount = preparedStatementCount.executeQuery();
            resultSetCount.next();
            int courseCount = resultSetCount.getInt(1);
            return courseCount;
        }catch (SQLException sqlException)
        {
           throw new DatabaseException(sqlException.getMessage());
        }
    }
```
<br>
In der Klasse Cli wird eine weitere Methode eingefügt.
Wichtig:
+   if(courseOptional.isPresent) überprüft ob zu dieser id ein Kurs gefunden wurde wird der Kurs ausgegeben mit **get()**.
+   

```java 
    private void showCourseDetails() {
        System.out.println("Für welchen Kurs möchten Sie die Kursdetails anzeigen? ");
        Long courseId = Long.parseLong(scan.nextLine());
        try
        {
            Optional<Course> courseOptional = repo.getById(courseId);
            if (courseOptional.isPresent())
            {
                System.out.println(courseOptional.get());
            } else {
                System.out.println("Kurs mit der Id " + courseId + " nicht gefunden!");
            }
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Kurs-Detailsanzeige: " + databaseException.getMessage());
        } catch (Exception exception)
        {
            System.out.println("Unbekannter Fehler bei Kurs-Detailanzeige: " + exception.getMessage());
        }
    }
```

<br>

## DAO Implementierung CRUD - CREATE
<br>

Als nächstes wird die CREATE bzw. insert Methode ausimplementiert und in der Cli Klasse eingefügt.

Diese Methode ist für den Eintrag neuer Datensätze verantwortlich.
Insert Statement ist bereits bekannt.
**Wichtig:**
+ Es wird geprüft, ob das Einfügen funktioniert hat --> affectedRows (wenn nicht wird ein leeres Optional zurückgegeben)
+ Falls es nicht 0 ist dann werden über das preparedStatement die getGeneratedKeys() geholt (können theoretisch auch mehrere sein bei mehreren Inserts)
+ Mit if(generatedKeys.next()) wird den Key an der stelle mit Long rausgeholt, sprich ein Datensatz mit einer Spalte wird zurückgegeben.
+ Und dies ist der verwendete Key, um die Entität zu speichern (return this.getById(generatedKeys.getLong(columnIndex: 1));
+ Weiteres wird mit diesem Key kann mit getByID Methode das neu generierte aus der Datenbank geholt werden und liefern dies als Optional vom Typ Course zurück.


```java
    @Override
    public Optional<Course> insert(Course entity) {
        Assert.notNull(entity);

        try
        {
            String sql = "INSERT INTO `courses` (`name`, `description`, `hours`, `begindate`, `enddate`, `coursetype`) VALUES (?,?,?,?,?,?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getDescription());
            preparedStatement.setInt(3, entity.getHours());
            preparedStatement.setDate(4, entity.getBeginDate());
            preparedStatement.setDate(5, entity.getEndDate());
            preparedStatement.setString(6, entity.getCourseType().toString());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0)
            {
                return Optional.empty();
            }
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next())
            {
                return this.getById(generatedKeys.getLong(1));
            } else
            {
                return Optional.empty();
            }
        }catch (SQLException sqlException)
        {
        throw new DatabaseException(sqlException.getMessage());
        }
    }
```
<br>

Diese Methode ist für das Hinzufügen neuer Kurse verantwortlich und dient gleichzeitig zur * *Validierung** der UI.
Es werden lokale Variablen deklariert.
In der Domänen klasse ist eine Validierung ebenfalls möglich, um nicht in einen inkonsistenten Zustand verfallen kann.
Dies kann selbst entschieden werden.
**Nach sämtlichen Prüfungen wird ein optional das Objekte vom Typ Course hält, ersellt und mit sämtlichen Daten gefüllt.**
Weiteres wird geprüft, ob ein Course angelegt wurde mit .isPresent().

```java
    private void addCourse() {

        String name, description;
        int hours;
        Date dateFrom, dateTo;
        CourseType courseType;

        try
        {
            System.out.println("Bitte alle Kursdaten angeben");
            System.out.println("Name: ");
            name = scan.nextLine();
            if (name.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            System.out.println("Beschreibung: ");
            description = scan.nextLine();
            if (description.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Stundenanzahl: ");
            hours = Integer.parseInt(scan.nextLine());
            System.out.println("Startdatum (YYY-MM-DD: ");
            dateFrom = Date.valueOf(scan.nextLine());
            System.out.println("Enddatum (YYY-MM-DD: ");
            dateTo = Date.valueOf(scan.nextLine());
            System.out.println("Kurstyp: (ZA/BF/FF/OE): ");
            courseType = CourseType.valueOf(scan.nextLine());

            Optional<Course> optionalCourse = repo.insert(
                    new Course(name,description,hours,dateFrom,dateTo,courseType)
            );
            if (optionalCourse.isPresent())
            {
                System.out.println("Kurs wurde angelegt: " + optionalCourse.get());
            } else
            {
                System.out.println("Kurs kontte nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException)
        {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException)
        {
            System.out.println("Kursdatum nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception)
        {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }
```

<br>

## DAO Implementierung CRUD - Update (Video 10)
<br>

In diesem Teil wird eine weitere CRUD Methode ausimplementiert!

Diese Methode dient zum Updaten der Des gewünschten Datensatzes und ist vom Aufbau sehr ähnlich zu den vorherigen Methoden.
Wichitg ist das alle Sql Parameter mit ? versehen sind!


```java 
    @Override
    public Optional<Course> update(Course entity) {

        Assert.notNull(entity);

        String sql = "UPDATE `courses` SET `name` =? , `description` = ?, `hours` = ?, `begindate` = ?, `enddate` = ? `coursetype` = ? WHERE `courses`.`id` = ?";

        if (countCoursesInDbWithId(entity.getId()) == 0)
        {
            return Optional.empty();
        } else {
            try {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setString(1, entity.getName());
                preparedStatement.setString(2, entity.getDescription());
                preparedStatement.setInt(3, entity.getHours());
                preparedStatement.setDate(4, entity.getBeginDate());
                preparedStatement.setDate(5, entity.getEndDate());
                preparedStatement.setString(6, entity.getCourseType().toString());
                preparedStatement.setLong(7, entity.getId());

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0){
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }

    }

```

**Cli Klasse**

Diese Methode dient zum Updaten von Datensätzen in der Datenbank und ist eine der CRUD Methoden.
Wichtig:
+ Der User gibt ein **welche ID **bzw. werlcher Course mit jener ID Upgedated werden soll sofern es diesen Course gibt
+ mit Course **course = courseOptional.get() wird jener Datensatz nach id ausgegeben welcher verändert wird.**
+ Alle eingaben entgegennehmen um den Datensatz zu ändern.
+ **Tertiärer Operator ? ist wie eine if-Bedingung**
+ name.equals("") ? course.getName() : name (Falls der Benutzer Enter drückt dafür die "", wird mit get.Name() der gespeicherte Name beibehalten, ansonsten werden der neu eingegebenen Name verwendet :name)
+ Bei allen weiteren Datenfelder erfolgt dasselbe!
+ bei optionalCourseUpdated.ifPresentOrElse
+ ist eine Funktion die Prüft ob der Course Present ist also ob jener Course verfügbar ist, trifft dies zu wird (c) -> ausgeführt(Present) (Das c ist der Methodenparameter und gibt uns den Course aus)    
  + Falls dies nicht zutrifft wird () -> ausgeführt (Else)

```java
    private void updateCourseDetails() {
        System.out.println("Für welchen Kurs-Id möchten Sie die Kursdetails ändern?");
        Long courseId = Long.parseLong(scan.nextLine());

        try {
            Optional<Course> courseOptional = repo.getById(courseId);
            if (courseOptional.isEmpty()){
                System.out.println("Kurs mit der gegebenen ID ist nicht in der Datenbank");
            } else {
                Course course = courseOptional.get();

                System.out.println("Änderungen für folgenden Kurs: ");
                System.out.println(course);

                String name, description, hours, dateFrom, dateTo, courseType;

                System.out.println("Bitte neue Kursdaten angeben (Enter, falls keine Änderung gewünscht ist)");

                System.out.println("Name: ");
                name = scan.nextLine();
                System.out.println("Beschreibung");
                description = scan.nextLine();
                System.out.println("Stundenzahl");
                hours = scan.nextLine();
                System.out.println("Startdatum (YYYY-MM-DD");
                dateFrom = scan.nextLine();
                System.out.println("Enddatum (YYYY-MM-DD");
                dateTo = scan.nextLine();
                System.out.println("Kurstyp (ZY/BF/FF/OE : ");
                courseType = scan.nextLine();

                Optional<Course> optionalCourseUpdated = repo.update(
                  new Course(
                          course.getId(),
                          name.equals("") ? course.getName() : name,
                          description.equals("") ? course.getDescription() : description,
                          hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                          dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                          dateTo.equals("") ? course.getEndDate() : Date.valueOf(dateTo),
                          courseType.equals("") ? course.getCourseType() : CourseType.valueOf(courseType)

                  )
                );

                optionalCourseUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Kurs aktualisiert " + c),
                        () -> System.out.println("Kurse konnte nicht aktualisiert werden!")
                );

            }
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Kursupdate: " + exception.getMessage());
        }

    }

```

<br>

## DAO Implementierung CRUD - DELETE (Video 11)
<br>

Die letzte CRUD Methode die noch ausimplementiert werden muss ist die DELETE.
Zuerst wird sie in der MySqlRepository definiert und mit einem Sql Statement versehen und anschließend wird in der Cli Klasse die deleteCourse() Methode ausimplementiert.
   
Die DELETE Methode prüft, ob die gewählte CourseId gleich 1 ist.
Trifft dies zu wird das PrepareStatement mit .executeUpdate() ausgeführt.
Zur Verbesserung sollte satt void etwas zurückgegeben werden ein Booleschen Wert z.B. ob gelöscht wurde oder  in der Cli Klasse.
     
```java

    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);

        String sql = "DELETE FROM `courses` WHERE `id` = ?";

        try {
            if (countCoursesInDbWithId(id) == 1) {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

``` 
  
Die deleteCourse Methode nimmt die Benutzereingabe entgegen und löscht auf Basis der eingegebenen ID.
    

```java
  
    private void deleteCourse() {
        System.out.println("Welchen Kurs möchten Sie läschen? Bitte ID eingeben: ");
        Long courseIdToDelete = Long.parseLong(scan.nextLine());

        try {
            repo.deleteById(courseIdToDelete);
            System.out.println("Kurs mit ID " + courseIdToDelete + " gelöscht!");
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());

        } catch (Exception e){
            System.out.println("Unbekannter Fehler beim Läschen: " + e.getMessage());
        }

    }
```
<br>

## DAO Implementierung Kurssuche (Video 12)

Nachdem alle CRUD Methoden implementiert wurden, ist nun die Zusatzmehtode aus dem MyCourseRepository Interface an der Reihe.

**Wichtig:**
+ Im SQL Statement wird mit LOWER nach der Beschreibung und name gesucht. Jeweils in Kleinbuchstaben.
+ Es werden neue Kurse in die ArrayList hinzugefügt mit den Werten aus dem ResultSet.
+ Anschließend wird die courseListe zurückgegeben bzw. nach außen gegeben.
    

```java

    @Override
    public List<Course> findAllCoursesByNameOrDescription(String searchText) {
        try{
            String sql = "SELECT * FROM `courses` WHERE LOWER(`description`) LIKE LOWER(?) OR LOWER(`name`) LIKE LOWER(?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%"+searchText+"%");
            preparedStatement.setString(2, "%"+searchText+"%");

            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Course> courseList = new ArrayList<>();

            while (resultSet.next()){
                courseList.add( new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("beginDate"),
                                resultSet.getDate("enddate"),
                                CourseType.valueOf(resultSet.getString("coursetype"))
                        ));
            }
            return courseList;
        }catch (SQLException sqlException){
          throw new DatabaseException(sqlException.getMessage());
        }

    }
```
<br>
Nun wird die Methode courseSearch() in der Cli Klasse implementiert
  
**Wichtig:**
+ Es wird eine neue Liste vom Typ Course erstellt.
+ Über das repo bekommt man die Methode findAllCoursesByNameOrDescription
+ Anschließend wird darüber itteriert und falls die Eingabe einen Treffer findet wird

```java

    private void courseSearch() {
        System.out.println("Geben Sie einen Suchbegriff an!");
        String searchString = scan.nextLine();
        List<Course> courseList;
        try {
            courseList = repo.findAllCoursesByNameOrDescription(searchString);
            for (Course course : courseList){
                System.out.println(course);
            }
        } catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Kurssuche: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche " + exception.getMessage());
        }
    }

```

<br>

## DAO Implementierung Aktuell laufende Kurse (Video 13)
<br>

Zum Abschluss wird noch eine weitere Methode namens findAllRunningCourses() ausimplementiert.


Wichtig:
+ WHERE NOW()<`enddate` liefert uns das aktuelle Datum somit muss nicht explizit in der Datenbank nach dem Course gesucht werden.
    
```java
    @Override
    public List<Course> findAllRunningCourses() {
        String sql = "SELECT * FROM `courses` WHERE NOW()<`enddate`";

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {
                courseList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("enddate"),
                        CourseType.valueOf(resultSet.getString("coursetype"))
                ));
            }
            return courseList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }
``` 
In der Cli Klasse

```java
    private void runningCourses() {
        System.out.println("Aktuell laufende Kurse: ");
        List<Course> list;
        try {

            list = repo.findAllRunningCourses();
            for (Course course : list){
                System.out.println(course);
            }
        } catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Kurs-Anzeige für laufende Kurse: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Kurs-Anzeige für laufende Kurse: " + exception.getMessage());
        }
    }
```

<br>

Fertige Ansicht alller Klassen und deren Verbindung

![Fertige-Ansicht](image%20courses/Fertige%20Ansciht.png)

<br>


# AUFGABE 4: JDBC UND DAO – STUDENTEN

Erweitere die fertig nachprogrammierte Applikation mit einem DAO für CRUD für eine neue Domänenklasse „Student“: 

+ Studenten haben einen eine Student-ID, einen VN, einen NN, ein Geburtsdatum
+ Domänenklasse implementieren (Setter absichern, neue Exceptions definieren, Business-Regeln selbst wählen - z.B. dass Name nicht leer sein darf)
+ Eigenes Interface MyStudentRepository von BaseRepository ableiten. MyStudentRepository muss mindestens 3 studentenspezifische Methoden enthalten (z.B. Studentensuche nach Namen, Suche nach ID, Suche nach bestimmtem Geburtsjahr, Suche mit Geburtsdatum zwischen x und y etc.).
+ Implementierung von MyStudentRepository durch eine neue Klasse MySqlStudentRepository analog zum MySqlCourseRepository.
+ Erweiterung des CLI für die Verarbeitung von Studenten und für spezifische Studenten-Funktionen (z.B. Student nach dem Namen suchen)


## Student Klasse

```java
package domain;

import java.sql.Date;

public class Student extends BaseEntity{


private String firstname;
private String lastname;
private Date dateofbirth;


    public Student(Long id, String firstname, String lastname, Date dateofbirth) {
        super(id);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setDateOfBirth(dateofbirth);
    }

    public Student(String firstname, String lastname, Date dateofbirth) {
        super(null);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setDateOfBirth(dateofbirth);
    }


    public void setFirstname(String firstname) {
        if (firstname != null && firstname.length() < 10 && !firstname.equals("")) {
            this.firstname = firstname;
        } else {
            throw new InvalidStudentException("Der Vorname darf nicht leer und maximal 10 Zeichen lang sein");
        }
    }


    public void setLastname(String lastname) {
      if (lastname != null && lastname.length() < 20 && !lastname.equals("")) {
          this.lastname = lastname;
      }else{
          throw new InvalidStudentException("Der Nachname darf nicht leer und maximal 20 Zeichen lang sein");
      }
    }

    public void setDateOfBirth(Date dateOfBirth) {
        if (dateOfBirth != null) {
            this.dateofbirth = dateOfBirth;
        }else {
            throw new InvalidStudentException("Geburtsdatum darf nicht leer sein");
        }
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Date getDateOfBirth() {
        return dateofbirth;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + this.getId() + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", dateOfBirth=" + dateofbirth +
                '}';
    }
}
``` 

<br>

## MySqlRepository Klasse


```java

package dataaccess;

import com.mysql.cj.protocol.Resultset;
import domain.Student;
import util.Assert;

import java.sql.*;
import java.util.*;
import java.sql.Date;

public class MySqlStudentRepository implements MyStudentRepository {

    Connection conn;


    public MySqlStudentRepository() throws SQLException, ClassNotFoundException {

        this.conn = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");
    }

    @Override
    public Optional<Student> insert(Student entity) {
        String sql = "INSERT INTO `students` (`firstname`, `lastname`, `dateofbirth`) VALUES (?,?,?)";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getFirstname());
            preparedStatement.setString(2, entity.getLastname());
            preparedStatement.setDate(3, entity.getDateOfBirth());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0){
                return Optional.empty();
            }
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()){
                return this.getById(generatedKeys.getLong(1));
            } else {
                return Optional.empty();
            }

        } catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }

    }

    @Override
    public Optional<Student> getById(Long id) {
        Assert.notNull(id);

        if (countStudentsInDbWithId(id) == 0){
            return Optional.empty();
        } else {
            try {
                String sql = "SELECT * FROM `students` WHERE `id`= ?";
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();

                resultSet.next();
                Student student = new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("dateofbirth")
                );
                return Optional.of(student);
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    private int countStudentsInDbWithId(Long id)
    {
        try {
            String countSql = "SELECT COUNT(*) FROM `students` WHERE `id`=?";
            PreparedStatement preparedStatementCount = conn.prepareStatement(countSql);
            preparedStatementCount.setLong(1, id);
            ResultSet resultSetCount = preparedStatementCount.executeQuery();
            resultSetCount.next();
            int studentCount = resultSetCount.getInt(1);
            return studentCount;
        }catch (SQLException sqlException)
        {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> getAll() {
       String sql = "SELECT * FROM `students`";
       try {
           PreparedStatement preparedStatement = conn.prepareStatement(sql);
           ResultSet resultSet = preparedStatement.executeQuery();

           ArrayList<Student> studentList =  new ArrayList<>();
           while (resultSet.next()){
               studentList.add( new Student(
                       resultSet.getLong("id"),
                       resultSet.getString("firstname"),
                       resultSet.getString("lastname"),
                       resultSet.getDate("dateofbirth"))
               );
           }
           return studentList;
       } catch (SQLException sqlException){
           throw new DatabaseException("Database error occurred");
       }
    }

    @Override
    public Optional<Student> update(Student entity) {

        Assert.notNull(entity);

        String sql = "UPDATE `students` SET `firstname` = ? , `lastname` = ?, `dateofbirth` = ? WHERE `students`.`id` = ?";

        if (countStudentsInDbWithId(entity.getId()) == 0){
            return Optional.empty();
        } else {
            try {
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setString(1,entity.getFirstname());
                preparedStatement.setString(2, entity.getLastname());
                preparedStatement.setDate(3, entity.getDateOfBirth());
                preparedStatement.setLong(4, entity.getId());

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0){
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch (SQLException sqlException){
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);

        String sql = "DELETE FROM `students` WHERE `id` = ?";

        try {
            if (countStudentsInDbWithId(id) == 1) {
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }


    @Override
    public List<Student> findAllStudentsByLastname(String lastname) {

        String sql = "SELECT * FROM `students` WHERE LOWER(`lastname`) LIKE (?)";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "%"+lastname+"%");

            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Student> studentList = new ArrayList<>();

            while (resultSet.next()){
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("dateofbirth")
                ));
            }
            return studentList;
        } catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByName(String name) {

        String sql = "SELECT * FROM `students` WHERE LOWER(`firstname`) Like LOWER(?) OR LOWER(`lastname`) LIKE LOWER(?)";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "%"+name+"%");
            preparedStatement.setString(2, "%"+name+"%");

            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Student> studentList = new ArrayList<>();

            while (resultSet.next()){
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("dateofbirth")
                ));
            }
            return studentList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }


    @Override
    public List<Student> findAllStudentsWithAgeBetween(Date ageRange) {

        String sql = "SELECT * FROM `students` WHERE `dateofbirth` BETWEEN ? AND ?";

        try {
            PreparedStatement preparedStatement =conn.prepareStatement(sql);
            preparedStatement.setDate(1, ageRange);
            preparedStatement.setDate(2, ageRange);

            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Student> studentList = new ArrayList<>();

            while (resultSet.next()){
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("dateofbirth")
                ));
            }
            return studentList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }
}
```

<br>

## MySqlRepository Interface

```java
package dataaccess;

import domain.Student;

import java.sql.Date;
import java.util.List;

public interface MyStudentRepository extends BaseRepository<Student, Long>{


    List<Student> findAllStudentsByLastname(String lastname);
    List<Student> findAllStudentsByName(String name);
    List<Student>  findAllStudentsWithAgeBetween(Date ageRange);
}
```

## Cli erweitert

```java
    private void studentByAge() {
        System.out.println("Geben Sie das Geburtsdatum des Studenten ein: Format (YYYY-MM-DD)");
        Date dateOfBirth = Date.valueOf(scan.nextLine());

        try{
            List<Student> studentList = studentRepo.findAllStudentsWithAgeBetween(dateOfBirth);
            for (Student student : studentList){
                System.out.println(student);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Suche nach dem Geburtstag");
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche nach Geburtstag");
        }    }

    private void studentByFullName() {

        System.out.println("Geben Sie den Vor- oder Nachnamen an!");
        String searchString = scan.nextLine();
        List<Student> studentList;
        try {
            studentList = studentRepo.findAllStudentsByName(searchString);
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Studentensuche: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei der Studentensuche: " + exception.getMessage());
        }
    }


    private void studentByLastname() {

        System.out.println("Geben Sie den Nachnamen des gesuchten Studenten ein, den Sie suchen");
        String searchLastname = scan.nextLine();
        List<Student> list;
        try{
            list = studentRepo.findAllStudentsByLastname(searchLastname);
            for (Student student : list){
                System.out.println(student);
            }

        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Kurs-Anzeige für gesuchten Kurs");
        }catch(Exception exception){
            System.out.println("Unbekannter Fehler beim Kursname");
        }
    }


    private void deleteStudent() {
        System.out.println("Welchen Student möchten Sie löschen? Bitte ID eingeben: ");
        Long studentIdToDelete = Long.parseLong(scan.nextLine());

        try {
            repo.deleteById(studentIdToDelete);
            System.out.println("Student mit ID " + studentIdToDelete + " gelöscht!");
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());

        } catch (Exception e){
            System.out.println("Unbekannter Fehler beim Löschen: " + e.getMessage());
        }
    }

    private void updateStudentDetails() {

        System.out.println("Für welche Studenten-ID sollen die Details geändert werden?");
        Long studentId = Long.parseLong(scan.nextLine());

        try {
            Optional<Student> studentOptional = studentRepo.getById(studentId);
            if (studentOptional.isEmpty()) {
                System.out.println("Student mit der gegebenen ID wurde nicht gefunden!");
            } else {
                Student student = studentOptional.get();

                System.out.println("Änderungen für folgende/n Studenten/in: ");
                System.out.println(student);

                String firstName, lastName, birthdate;

                System.out.println("Bitte Studentendaten aktualisieren (Enter, falls keine Änderung gewünscht ist):");
                System.out.println("Vorname: ");
                firstName = scan.nextLine();
                System.out.println("Nachname: ");
                lastName = scan.nextLine();
                System.out.println("Geburtsdatum (YYYY-MM-DD): ");
                birthdate = scan.nextLine();

                Optional<Student> optionalStudentUpdated = studentRepo.update(
                        new Student(
                                student.getId(),
                                firstName.equals("") ? student.getFirstname() : firstName,
                                lastName.equals("") ? student.getLastname() : lastName,
                                birthdate.equals("") ? student.getDateOfBirth() : Date.valueOf(birthdate)
                        )
                );

                optionalStudentUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Student/in aktualisiert: " + c),
                        () -> System.out.println("Student/in konnte nicht aktualisiert werden!")
                );
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim einfügen: " + exception.getMessage());
        }


    }

    private void showStudentDetails() {

        System.out.println("Für welchen Student sollen die Details angezeigt werden? ");
        Long studentId = Long.parseLong(scan.nextLine());
        try
        {
            Optional<Student> studentOptional = studentRepo.getById(studentId);
            if (studentOptional.isPresent())
            {
                System.out.println(studentOptional.get());
            } else {
                System.out.println("Student mit der Id " + studentId + " nicht gefunden!");
            }
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Student-Detailanzeige: " + databaseException.getMessage());
        } catch (Exception exception)
        {
            System.out.println("Unbekannter Fehler bei Student-Detailanzeige: " + exception.getMessage());
        }
    }

    private void showAllStudents() {
        List<Student> list = null;

        try{
            list = studentRepo.getAll();
            if (list.size()>0)
            {
                for (Student student: list)
                {
                    System.out.println(student);
                }
            } else {
                System.out.println("Studentenliste ist leer!");
            }
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Anzeige aller Studenten: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Studenten: " + exception.getMessage());
        }
    }


    private void addStudent() {

        String firstname, lastname;
        Date dateofbirth;

        try {
            System.out.println("Bitte Vorname, Nachname und Geburtsdatum eingeben:");
            System.out.println("Vorname: ");
            firstname = scan.nextLine();
            if (firstname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Nachname: ");
            lastname = scan.nextLine();
            if (lastname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Geburtsdatum (YYYY-MM-DD): ");
            dateofbirth = Date.valueOf(scan.nextLine());


            Optional<Student> optionalStudent = studentRepo.insert(
                    new Student(firstname, lastname, dateofbirth)
            );
            if (optionalStudent.isPresent()) {
                System.out.println("Student angelegt: " + optionalStudent.get());
            } else {
                System.out.println("Student konnte nicht angelegt werden! ");
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim einfügen: " + exception.getMessage());
        }
    }

``` 

# AUFGABE 5: JDBC UND DAO – BUCHUNGEN 

+ Gib einen textuellen Vorschlag hab, wie man die bisher programmierte Applikation für die Buchung von Kursen durch Studenten erweitern könnte.  
+ Beschreibe, wie eine neue Buchungs-Domänenklasse ausschauen sollte, wie man ein DAO für Buchungen dazu entwickeln sollte, wie man die CLI anpassen müsste und welche Anwendungsfälle der Benutzer brauchen könnte (wie etwa „Buchung erstellen“). 
+ Verwende zur Illustration insb. auch UML-Diagramme. 

<br>

**Vorschlag:**

Eine Möglichkeit, um Buchungen darzustellen, wäre eine eigene Tabelle namens Buchungen in meinem Fall bookings zu erstellen. Diese Tabelle dient als Zwischentabelle(Intersektio), da es sich bei der Beziehung zwischen courses und students um eine n:m handelt. Sie besitzt eine id sowie die Fremdschlüssel von courses und students. Dadurch können mittels join beliebige Daten angezeigt werden.

**die Implementierung:**

Package Dataaccess:
+ Eine neue Klasse namens MySqlBookingRepository welche die Methoden vom MyBookingRepository implementier.
+ Eine neues Interface namens MyBookingRepository das vom Interface BaseRepository erbt welches die CRUD Methoden zur Verfügung stellt.

Package domain:
+ Eine neue Klasse namens Booking welche von der abstrakten Klasse BaseEntity erbt.
+ Gegebenfalls eine weitere Exception Klasse speziell für die Methoden die von Booking ausgehen.

Package ui:
+ Erweiterung der Cli Klasse mit den neuen Booking Mehtoden.

![Booking](images/Booking.png)