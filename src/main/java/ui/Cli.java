package ui;

import dataaccess.DatabaseException;
import dataaccess.MyCourseRepository;
import dataaccess.MySqlStudentRepository;
import dataaccess.MyStudentRepository;
import domain.Course;
import domain.CourseType;
import domain.InvalidValueException;
import domain.Student;

import javax.xml.crypto.Data;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Cli {

    Scanner scan;
    MyCourseRepository repo;
    MyStudentRepository studentRepo;

    public Cli(MyCourseRepository repo, MyStudentRepository studentRepo)
    {
        this.scan = new Scanner(System.in);
        this.repo = repo;
        this.studentRepo = studentRepo;
    }


    public void start()
    {
        String input = "-";
        while (!input.equals("x"))
        {
            showMenue();
            input = scan.nextLine();
            switch(input){
                case "1":
                    addCourse();
                    break;
                case "2":
                    showAllCourses();
                    break;
                case "3":
                    showCourseDetails();
                    break;
                case "4":
                    updateCourseDetails();
                    break;
                case "5":
                    deleteCourse();
                    break;
                case "6":
                    courseSearch();
                    break;
                case "7":
                    runningCourses();
                    break;
                case "8":
                    courseByName();
                    break;
                case "9":
                    courseByDescription();
                    break;
                case "10":
                    courseByType();
                    break;
                case "11":
                    courseByStartDate();
                    break;
                case "12":
                    addStudent();
                    break;
                case "13":
                    showAllStudents();
                    break;
                case "14":
                    showStudentDetails();
                    break;
                case "15":
                    updateStudentDetails();
                    break;
                case "16":
                    deleteStudent();
                    break;
                case "17":
                    studentByLastname();
                    break;
                case "18":
                    studentByFullName();
                    break;
                case "19":
                    studentByAge();
                    break;
                case "x":
                    System.out.println("Auf Wiedersehen!");
                    break;
                default:
                    inputError();
                    break;
            }
        }
        scan.close();
    }

    private void studentByAge() {
        System.out.println("Geben Sie das Geburtsdatum des Studenten ein: Format (YYYY-MM-DD)");
        Date dateOfBirth = Date.valueOf(scan.nextLine());

        try{
            List<Student> studentList = studentRepo.findAllStudentsWithAgeBetween(dateOfBirth);
            for (Student student : studentList){
                System.out.println(student);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Suche nach dem Geburtstag");
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche nach Geburtstag");
        }    }

    private void studentByFullName() {

        System.out.println("Geben Sie den Vor- oder Nachnamen an!");
        String searchString = scan.nextLine();
        List<Student> studentList;
        try {
            studentList = studentRepo.findAllStudentsByName(searchString);
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Studentensuche: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei der Studentensuche: " + exception.getMessage());
        }
    }


    private void studentByLastname() {

        System.out.println("Geben Sie den Nachnamen des gesuchten Studenten ein, den Sie suchen");
        String searchLastname = scan.nextLine();
        List<Student> list;
        try{
            list = studentRepo.findAllStudentsByLastname(searchLastname);
            for (Student student : list){
                System.out.println(student);
            }

        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Kurs-Anzeige für gesuchten Kurs");
        }catch(Exception exception){
            System.out.println("Unbekannter Fehler beim Kursname");
        }
    }


    private void deleteStudent() {
        System.out.println("Welchen Student möchten Sie löschen? Bitte ID eingeben: ");
        Long studentIdToDelete = Long.parseLong(scan.nextLine());

        try {
            repo.deleteById(studentIdToDelete);
            System.out.println("Student mit ID " + studentIdToDelete + " gelöscht!");
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());

        } catch (Exception e){
            System.out.println("Unbekannter Fehler beim Löschen: " + e.getMessage());
        }
    }

    private void updateStudentDetails() {

        System.out.println("Für welche Studenten-ID sollen die Details geändert werden?");
        Long studentId = Long.parseLong(scan.nextLine());

        try {
            Optional<Student> studentOptional = studentRepo.getById(studentId);
            if (studentOptional.isEmpty()) {
                System.out.println("Student mit der gegebenen ID wurde nicht gefunden!");
            } else {
                Student student = studentOptional.get();

                System.out.println("Änderungen für folgende/n Studenten/in: ");
                System.out.println(student);

                String firstName, lastName, birthdate;

                System.out.println("Bitte Studentendaten aktualisieren (Enter, falls keine Änderung gewünscht ist):");
                System.out.println("Vorname: ");
                firstName = scan.nextLine();
                System.out.println("Nachname: ");
                lastName = scan.nextLine();
                System.out.println("Geburtsdatum (YYYY-MM-DD): ");
                birthdate = scan.nextLine();

                Optional<Student> optionalStudentUpdated = studentRepo.update(
                        new Student(
                                student.getId(),
                                firstName.equals("") ? student.getFirstname() : firstName,
                                lastName.equals("") ? student.getLastname() : lastName,
                                birthdate.equals("") ? student.getDateOfBirth() : Date.valueOf(birthdate)
                        )
                );

                optionalStudentUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Student/in aktualisiert: " + c),
                        () -> System.out.println("Student/in konnte nicht aktualisiert werden!")
                );
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim einfügen: " + exception.getMessage());
        }


    }

    private void showStudentDetails() {

        System.out.println("Für welchen Student sollen die Details angezeigt werden? ");
        Long studentId = Long.parseLong(scan.nextLine());
        try
        {
            Optional<Student> studentOptional = studentRepo.getById(studentId);
            if (studentOptional.isPresent())
            {
                System.out.println(studentOptional.get());
            } else {
                System.out.println("Student mit der Id " + studentId + " nicht gefunden!");
            }
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Student-Detailanzeige: " + databaseException.getMessage());
        } catch (Exception exception)
        {
            System.out.println("Unbekannter Fehler bei Student-Detailanzeige: " + exception.getMessage());
        }
    }

    private void showAllStudents() {
        List<Student> list = null;

        try{
            list = studentRepo.getAll();
            if (list.size()>0)
            {
                for (Student student: list)
                {
                    System.out.println(student);
                }
            } else {
                System.out.println("Studentenliste ist leer!");
            }
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Anzeige aller Studenten: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Studenten: " + exception.getMessage());
        }
    }


    private void addStudent() {

        String firstname, lastname;
        Date dateofbirth;

        try {
            System.out.println("Bitte Vorname, Nachname und Geburtsdatum eingeben:");
            System.out.println("Vorname: ");
            firstname = scan.nextLine();
            if (firstname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Nachname: ");
            lastname = scan.nextLine();
            if (lastname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Geburtsdatum (YYYY-MM-DD): ");
            dateofbirth = Date.valueOf(scan.nextLine());


            Optional<Student> optionalStudent = studentRepo.insert(
                    new Student(firstname, lastname, dateofbirth)
            );
            if (optionalStudent.isPresent()) {
                System.out.println("Student angelegt: " + optionalStudent.get());
            } else {
                System.out.println("Student konnte nicht angelegt werden! ");
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim einfügen: " + exception.getMessage());
        }
    }

    private void courseByStartDate() {
        System.out.println("Geben Sie das Startdatum des Kurses ein: Format (YYYY-MM-DD)");
        Date startdate = Date.valueOf(scan.nextLine());

        try{
            List<Course> courseList = repo.findAllCoursesByStartDate(startdate);
            for (Course course : courseList){
                System.out.println(course);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Suche nach Startdatum");
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche nach Startdatum");
        }
    }

    private void courseByType() {
        System.out.println("Geben Sie den Typ des gewünschten Kurses ein!");
        CourseType searchType = CourseType.valueOf(scan.nextLine());

        try {
            List<Course> courseList = repo.findAllCoursesByCourseType(searchType);
            for (Course course : courseList){
                System.out.println(course);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Suche nach dem Kurstyp");
        } catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Suche");
        }
    }



    private void courseByDescription() {
        System.out.println("Geben Sie die Beschreibung eines Kurses an um Danach zu suche! ");
        String descriptionSearch = scan.nextLine();

        try {
            List<Course> courseList = repo.findAllCoursesByDescription(descriptionSearch);
            for (Course course : courseList) {
                System.out.println(course);
            }
        }catch (DatabaseException databaseException ){
            System.out.println("Datenbankfehler!");
        } catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche");
        }
    }



    private void courseByName() {
        System.out.println("Geben Sie den Namen eines Kurses ein, den Sie suchen");
        String searchName = scan.nextLine();
        List<Course> list;
        try{
            list = repo.findAllCoursesByName(searchName);
            for (Course course : list){
                System.out.println(course);
            }

        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Kurs-Anzeige für gesuchten Kurs");
        }catch(Exception exception){
            System.out.println("Unbekannter Fehler beim Kursname");
        }
    }

    private void runningCourses() {
        System.out.println("Aktuell laufende Kurse: ");
        List<Course> list;
        try {

            list = repo.findAllRunningCourses();
            for (Course course : list){
                System.out.println(course);
            }
        } catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Kurs-Anzeige für laufende Kurse: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Kurs-Anzeige für laufende Kurse: " + exception.getMessage());
        }
    }


    private void courseSearch() {
        System.out.println("Geben Sie einen Suchbegriff an!");
        String searchString = scan.nextLine();
        List<Course> courseList;
        try {
            courseList = repo.findAllCoursesByNameOrDescription(searchString);
            for (Course course : courseList){
                System.out.println(course);
            }
        } catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Kurssuche: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche " + exception.getMessage());
        }
    }

    /*
    Die deleteCourse Methode nimmt die Benutzereingabe entgegen und löscht auf Basis der eingegebenen ID.
     */
    private void deleteCourse() {
        System.out.println("Welchen Kurs möchten Sie löschen? Bitte ID eingeben: ");
        Long courseIdToDelete = Long.parseLong(scan.nextLine());

        try {
            repo.deleteById(courseIdToDelete);
            System.out.println("Kurs mit ID " + courseIdToDelete + " gelöscht!");
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());

        } catch (Exception e){
            System.out.println("Unbekannter Fehler beim Läschen: " + e.getMessage());
        }

    }


    private void updateCourseDetails() {
        System.out.println("Für welchen Kurs-Id möchten Sie die Kursdetails ändern?");
        Long courseId = Long.parseLong(scan.nextLine());

        try {
            Optional<Course> courseOptional = repo.getById(courseId);
            if (courseOptional.isEmpty()){
                System.out.println("Kurs mit der gegebenen ID ist nicht in der Datenbank");
            } else {
                Course course = courseOptional.get();

                System.out.println("Änderungen für folgenden Kurs: ");
                System.out.println(course);

                String name, description, hours, dateFrom, dateTo, courseType;

                System.out.println("Bitte neue Kursdaten angeben (Enter, falls keine Änderung gewünscht ist)");

                System.out.println("Name: ");
                name = scan.nextLine();
                System.out.println("Beschreibung");
                description = scan.nextLine();
                System.out.println("Stundenzahl");
                hours = scan.nextLine();
                System.out.println("Startdatum (YYYY-MM-DD");
                dateFrom = scan.nextLine();
                System.out.println("Enddatum (YYYY-MM-DD");
                dateTo = scan.nextLine();
                System.out.println("Kurstyp (ZY/BF/FF/OE : ");
                courseType = scan.nextLine();

                Optional<Course> optionalCourseUpdated = repo.update(
                  new Course(
                          course.getId(),
                          name.equals("") ? course.getName() : name,
                          description.equals("") ? course.getDescription() : description,
                          hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                          dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                          dateTo.equals("") ? course.getEndDate() : Date.valueOf(dateTo),
                          courseType.equals("") ? course.getCourseType() : CourseType.valueOf(courseType)

                  )
                );

                optionalCourseUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Kurs aktualisiert " + c),
                        () -> System.out.println("Kurse konnte nicht aktualisiert werden!")
                );

            }
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Kursupdate: " + exception.getMessage());
        }

    }




    /*
    Diese Methode ist für das Hinzufügen neuer Kurse verantwortlich und dient gleichzeitig zur Validierung der UI.
    In der Domänen klasse ist eine Validierung ebenfalls möglich, um nicht in einen inkonsistenten Zustand verfallen kann.
    Dies kann selbst entschieden werden.
    Nach sämtlichen Prüfungen wird ein Optional das Objekte vom Typ Course hält ersellt und mit sämtlichen Daten gefüllt.
    Weiteres wird geprüft ob ein Course angelegt wurde mit.isPresent().
     */
    private void addCourse() {

        String name, description;
        int hours;
        Date dateFrom, dateTo;
        CourseType courseType;

        try
        {
            System.out.println("Bitte alle Kursdaten angeben");
            System.out.println("Name: ");
            name = scan.nextLine();
            if (name.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            System.out.println("Beschreibung: ");
            description = scan.nextLine();
            if (description.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Stundenanzahl: ");
            hours = Integer.parseInt(scan.nextLine());
            System.out.println("Startdatum (YYY-MM-DD: ");
            dateFrom = Date.valueOf(scan.nextLine());
            System.out.println("Enddatum (YYY-MM-DD: ");
            dateTo = Date.valueOf(scan.nextLine());
            System.out.println("Kurstyp: (ZA/BF/FF/OE): ");
            courseType = CourseType.valueOf(scan.nextLine());

            Optional<Course> optionalCourse = repo.insert(
                    new Course(name,description,hours,dateFrom,dateTo,courseType)
            );
            if (optionalCourse.isPresent())
            {
                System.out.println("Kurs wurde angelegt: " + optionalCourse.get());
            } else
            {
                System.out.println("Kurs konnte nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException)
        {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException)
        {
            System.out.println("Kursdatum nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception)
        {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void showCourseDetails() {
        System.out.println("Für welchen Kurs möchten Sie die Kursdetails anzeigen? ");
        Long courseId = Long.parseLong(scan.nextLine());
        try
        {
            Optional<Course> courseOptional = repo.getById(courseId);
            if (courseOptional.isPresent())
            {
                System.out.println(courseOptional.get());
            } else {
                System.out.println("Kurs mit der Id " + courseId + " nicht gefunden!");
            }
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Kurs-Detailsanzeige: " + databaseException.getMessage());
        } catch (Exception exception)
        {
            System.out.println("Unbekannter Fehler bei Kurs-Detailanzeige: " + exception.getMessage());
        }
    }

    private void showAllCourses() {

        List<Course> list = null;

        try{
        list = repo.getAll();
        if (list.size()>0)
        {
         for (Course course : list)
         {
             System.out.println(course);
         }
        } else {
            System.out.println("Kursliste leer!");
        }
    } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Anzeige aller Kurse: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Kurse: " + exception.getMessage());
        }
    }


    private void showMenue()
    {
        System.out.println("------------------------ KURSMANAGEMENT ------------------------");
        System.out.println("(1) Kurs eingeben \t (2) Alle Kurse anzeigen \t" + "(3) Kursdetails anzeigen");
        System.out.println("(4) Kursdetails ändern \t (5) Kurs löschen \t" + "(6) Kurssuche");
        System.out.println("(7) Laufende Kurse  \t (8) Kurssuche nach Namen \t" + "(9) Kurssuche nach Beschreibung");
        System.out.println("(10) Kurssuche nach Typ  \t (11) Kurssuche nach Startdatum \t" + "(12) Student hinzufügen");
        System.out.println("(13) Alle Studenten anzeigen \t (14) Studentendetails anzeigen \t" + "(15) Studentendetails ändern");
        System.out.println("(16) Student löschen \t (17) Studentensuche nach Nachname \t" + "(18) Studentensuche mit vollständigen Namen");
        System.out.println("(19) Studenten nach alter suchen");
        System.out.println("(x) Ende");
    }


    private void inputError()
    {
        System.out.println("Bitte nur die Zahlen der Menüauswahl eingeben");
    }
}
