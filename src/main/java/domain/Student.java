package domain;

import java.sql.Date;

public class Student extends BaseEntity{


private String firstname;
private String lastname;
private Date dateofbirth;


    public Student(Long id, String firstname, String lastname, Date dateofbirth) {
        super(id);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setDateOfBirth(dateofbirth);
    }

    public Student(String firstname, String lastname, Date dateofbirth) {
        super(null);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setDateOfBirth(dateofbirth);
    }


    public void setFirstname(String firstname) {
        if (firstname != null && firstname.length() < 10 && !firstname.equals("")) {
            this.firstname = firstname;
        } else {
            throw new InvalidStudentException("Der Vorname darf nicht leer und maximal 10 Zeichen lang sein");
        }
    }


    public void setLastname(String lastname) {
      if (lastname != null && lastname.length() < 20 && !lastname.equals("")) {
          this.lastname = lastname;
      }else{
          throw new InvalidStudentException("Der Nachname darf nicht leer und maximal 20 Zeichen lang sein");
      }
    }

    public void setDateOfBirth(Date dateOfBirth) {
        if (dateOfBirth != null) {
            this.dateofbirth = dateOfBirth;
        }else {
            throw new InvalidStudentException("Geburtsdatum darf nicht leer sein");
        }
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Date getDateOfBirth() {
        return dateofbirth;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + this.getId() + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", dateOfBirth=" + dateofbirth +
                '}';
    }
}


