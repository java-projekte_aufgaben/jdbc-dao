package dataaccess;

import domain.Student;

import java.sql.Date;
import java.util.List;

public interface MyStudentRepository extends BaseRepository<Student, Long>{


    List<Student> findAllStudentsByLastname(String lastname);
    List<Student> findAllStudentsByName(String name);
    List<Student>  findAllStudentsWithAgeBetween(Date ageRange);


}
