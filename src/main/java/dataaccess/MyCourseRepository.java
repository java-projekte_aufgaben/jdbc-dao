package dataaccess;

import domain.Course;
import domain.CourseType;

import java.sql.Date;
import java.util.List;

// Dieses Interface dient als zusatz wo einerseits die Methoden des BaseRepository implementiert werden und andererseits können eigene Zusatzmethoden definiert werden
// Das BaseRepository dient als CRUD REPO deshalb wir erst in dieser Klasse typisiert!!
// Statt T jetzt Courses und I ist Long. Abhängigkeiten werden vermieden und im weiteren Verlauf kann.
public interface MyCourseRepository extends BaseRepository <Course, Long>{

    List<Course> findAllCoursesByName(String name);
    List<Course> findAllCoursesByDescription(String description);
    List<Course> findAllCoursesByNameOrDescription(String searchText);
    List<Course> findAllCoursesByCourseType(CourseType courseType);
    List<Course> findAllCoursesByStartDate(Date startDate);
    List<Course> findAllRunningCourses();
}
