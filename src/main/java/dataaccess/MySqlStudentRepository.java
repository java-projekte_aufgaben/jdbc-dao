package dataaccess;

import domain.Student;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlStudentRepository implements MyStudentRepository {

    Connection conn;


    public MySqlStudentRepository() throws SQLException, ClassNotFoundException {

        this.conn = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");
    }

    @Override
    public Optional<Student> insert(Student entity) {
        String sql = "INSERT INTO `students` (`firstname`, `lastname`, `dateofbirth`) VALUES (?,?,?)";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getFirstname());
            preparedStatement.setString(2, entity.getLastname());
            preparedStatement.setDate(3, entity.getDateOfBirth());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0){
                return Optional.empty();
            }
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()){
                return this.getById(generatedKeys.getLong(1));
            } else {
                return Optional.empty();
            }

        } catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }

    }

    @Override
    public Optional<Student> getById(Long id) {
        Assert.notNull(id);

        if (countStudentsInDbWithId(id) == 0){
            return Optional.empty();
        } else {
            try {
                String sql = "SELECT * FROM `students` WHERE `id`= ?";
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();

                resultSet.next();
                Student student = new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("dateofbirth")
                );
                return Optional.of(student);
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    private int countStudentsInDbWithId(Long id)
    {
        try {
            String countSql = "SELECT COUNT(*) FROM `students` WHERE `id`=?";
            PreparedStatement preparedStatementCount = conn.prepareStatement(countSql);
            preparedStatementCount.setLong(1, id);
            ResultSet resultSetCount = preparedStatementCount.executeQuery();
            resultSetCount.next();
            int studentCount = resultSetCount.getInt(1);
            return studentCount;
        }catch (SQLException sqlException)
        {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> getAll() {
       String sql = "SELECT * FROM `students`";
       try {
           PreparedStatement preparedStatement = conn.prepareStatement(sql);
           ResultSet resultSet = preparedStatement.executeQuery();

           ArrayList<Student> studentList =  new ArrayList<>();
           while (resultSet.next()){
               studentList.add( new Student(
                       resultSet.getLong("id"),
                       resultSet.getString("firstname"),
                       resultSet.getString("lastname"),
                       resultSet.getDate("dateofbirth"))
               );
           }
           return studentList;
       } catch (SQLException sqlException){
           throw new DatabaseException("Database error occurred");
       }
    }

    @Override
    public Optional<Student> update(Student entity) {

        Assert.notNull(entity);

        String sql = "UPDATE `students` SET `firstname` = ? , `lastname` = ?, `dateofbirth` = ? WHERE `students`.`id` = ?";

        if (countStudentsInDbWithId(entity.getId()) == 0){
            return Optional.empty();
        } else {
            try {
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setString(1,entity.getFirstname());
                preparedStatement.setString(2, entity.getLastname());
                preparedStatement.setDate(3, entity.getDateOfBirth());
                preparedStatement.setLong(4, entity.getId());

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0){
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch (SQLException sqlException){
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);

        String sql = "DELETE FROM `students` WHERE `id` = ?";

        try {
            if (countStudentsInDbWithId(id) == 1) {
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }


    @Override
    public List<Student> findAllStudentsByLastname(String lastname) {

        String sql = "SELECT * FROM `students` WHERE LOWER(`lastname`) LIKE (?)";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "%"+lastname+"%");

            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Student> studentList = new ArrayList<>();

            while (resultSet.next()){
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("dateofbirth")
                ));
            }
            return studentList;
        } catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByName(String name) {

        String sql = "SELECT * FROM `students` WHERE LOWER(`firstname`) Like LOWER(?) OR LOWER(`lastname`) LIKE LOWER(?)";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "%"+name+"%");
            preparedStatement.setString(2, "%"+name+"%");

            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Student> studentList = new ArrayList<>();

            while (resultSet.next()){
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("dateofbirth")
                ));
            }
            return studentList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }


    @Override
    public List<Student> findAllStudentsWithAgeBetween(Date ageRange) {

        String sql = "SELECT * FROM `students` WHERE `dateofbirth` BETWEEN ? AND ?";

        try {
            PreparedStatement preparedStatement =conn.prepareStatement(sql);
            preparedStatement.setDate(1, ageRange);
            preparedStatement.setDate(2, ageRange);

            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Student> studentList = new ArrayList<>();

            while (resultSet.next()){
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("dateofbirth")
                ));
            }
            return studentList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }
}
